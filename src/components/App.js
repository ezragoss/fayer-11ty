
const { html } = require('htm/preact');

module.exports = ({ page }) => {
    var name = "";
    var data = "";
    if (page) { name = page.name; data = page.data } else { name = "Home"; data = "WOOOOO" };
    return html`<div class="App"><h1>${name}</h1>${data}</div>`
};
